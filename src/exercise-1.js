const getCommonItems = (array1, array2) => {
  // implement code here
  const result = [];
  for(const number1 of array1) {
    for(const number2 of array2) {
      if(number1 === number2 && !result.includes(number1)) {
        result.push(number1);
      }
    }
  }
  return result;
};

export default getCommonItems;
