const getTheExactNumber = (numbers) => {
  // implement code here
  let result = undefined;
  numbers.forEach(element => {
    if(element % 3 === 0 && (result === undefined || element > result)) {
        result = element;
    }
  });
  return result;
}

export default getTheExactNumber;